package com.test.rochaktathya.unknown.factsin.hindi

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
